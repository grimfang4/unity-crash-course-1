using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 10f;
    public float jumpSpeed = 20f;

    public GameObject target;

    [SerializeField] private GameObject bullet;


    private void Awake()
    {
        // Use this if you need to get set up before Start()
    }

    // Start is called before the first frame update
    void Start()
    {
        //transform.position = new Vector3(0, 4, 2);
    }

    // Update is called once per frame
    void Update()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 velocity = new Vector3(horiz, 0, vert);
        velocity.Normalize();

        transform.position += velocity * speed * Time.deltaTime;


        if(Input.GetButtonDown("Fire1"))
        {
            GameObject go = Instantiate(bullet, transform.position, transform.rotation);
            Rigidbody rb = go.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.velocity = transform.rotation * new Vector3(0, 0, 100);
            }
        }

        if(Input.GetButton("Jump"))
        {
            transform.position += new Vector3(0, 1, 0) * jumpSpeed * Time.deltaTime;
        }

        Vector3 separation = target.transform.position - transform.position;
        separation.Normalize();

        transform.rotation = Quaternion.LookRotation(separation, Vector3.up);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Ball"))
        {
            Debug.Log("Kicked the ball...");
        }
    }
}
